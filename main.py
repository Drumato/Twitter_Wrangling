import tweepy
from streamer import StreamListener
from tqdm import tqdm
CONSUMER_KEY = 'brIF4W38SgyDaoNN5CeQLdb6m'
CONSUMER_SECRET = 'FoXyc5I0bI3JS2nYqyT0LsnrjrqLY5P8TKJrjpQUgaSgVZ9jZ7'
ACCESS_TOKEN = '1021921129034706944-zd7BFBvV9qk3F96pn1Fjz7o0fY1YZV'
ACCESS_SECRET = 'kptify1z66rNoj8wb6NAMaakbgEh0RUCXOVQpwMxeg2Ph'

def prepareusingapi() -> 'api':
    auth = tweepy.OAuthHandler(CONSUMER_KEY,CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN,ACCESS_SECRET)
    return tweepy.API(auth,wait_on_rate_limit= True)

api = prepareusingapi()

def collectbykeyword(keyword:str,dfile:str):
    #results = [tweet.full_text+'\n' 
    #        for tweet in tqdm(tweepy.Cursor(twitterapi.search,q=keyword,count=100,tweet_mode='extended').items())]
    tweets_data=[]
    for tweet in tweepy.Cursor(api.search, q=keyword, count=100,tweet_mode='extended').items():
        tweets_data.append(tweet.full_text + '\n')
    fname = r"'"+ dfile + "'"
    fname = fname.replace("'","")

    with open(fname, "w",encoding="utf-8") as f:
        f.writelines(tweets_data)

def getuserinfo(screen_name:str) -> 'userinfo':
    return  api.get_user(screen_name=screen_name)

def printuserinfo():
    user_info = getuserinfo('drumato')
    print(f'【名前】{user_info.name}')
    print(f'【アカウント名】{user_info.screen_name}')
    print(f'【自己紹介】{user_info.description}')
    print(f'【フォロー数】:{user_info.friends_count}')
    print(f'【フォロワー数】:{user_info.followers_count}')


    
def createstream() -> 'streamobject':
    listener = StreamListener()
    stream = tweepy.Stream(auth = api.auth,listener=listener())
    return stream


if __name__ == '__main__':
    stream = createstream()

